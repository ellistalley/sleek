# Sleek

An AJAX driven theme for [Ghost](http://github.com/tryghost/ghost/) by [Ellis Talley](https://gitlab.com/ellistalley/sleek).
Forked from Bleak by [Peter Amende](https://github.com/zutrinken/bleak).

## Demo
* [Demo](http://ellistalley.com)

## Features
* Static home page
* Sleek transition to other pages
* Simple and minimalistic
* Responsive layout
* Automatic code syntax highlight and line numbers
* Demonstation canvases for 2D/3D Javascript/Unity3D

## Installation
Either clone or download a zipped file and upload to your Ghost install. You then need to either override or symbolic link the routes.yaml file in the "routes" directory into the settings folder of your Ghost install.

## Screenshots
<table>
<tr>
<td valign="top">
<img src="https://raw.githubusercontent.com/zutrinken/bleak/master/src/screenshot-desktop.jpg" />
</td>
<td valign="top">
<img src="https://raw.githubusercontent.com/zutrinken/bleak/master/src/screenshot-mobile.jpg" />
</td>
</tr>
</table>

## Development
Install [Grunt](http://gruntjs.com/getting-started/):

	npm install -g grunt-cli

Install Grunt modules:

	npm install

Install [Bower](http://bower.io):

	npm install -g bower

Install Bower components:

	bower install

Build Grunt project:

	grunt

Distribute Grunt project:

	grunt build

## Copyright & License
Copyright (C) 2019 Ellis Talley - Released under the [MIT License](https://gitlab.com/ellistalley/sleek/blob/master/LICENSE.txt).
